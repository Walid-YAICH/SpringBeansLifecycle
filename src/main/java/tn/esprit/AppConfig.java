/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */

package tn.esprit;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;

@Configuration
@ComponentScan(basePackages={"tn.esprit"})
@EnableAspectJAutoProxy
public class AppConfig {
	
	 //you do not need to declare this bean for @propertySource
	//please see logs with debug level or run this application in debug mode.
	//This is a bean factory post processor
	//https://github.com/spring-projects/spring-framework/commit/f55a6051dfe0ea51fc82196cb6ca3c9a4e1570ff
	//https://stackoverflow.com/questions/54987934/spring-bean-lifecycle-value-attributes-are-null-in-the-constructor
//	 @Bean
//	 public static PropertySourcesPlaceholderConfigurer placeHolderConfigurer() {
//		return new PropertySourcesPlaceholderConfigurer();
//	 }
	
}

