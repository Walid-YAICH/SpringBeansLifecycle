package tn.esprit;
/*
 * Copyright 2018 by Walid YAICH <walid.yaich@esprit.tn>
 * This is an Open Source Software
 * License: http://www.gnu.org/licenses/gpl.html GPL version 3
 */



import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

//https://stackoverflow.com/questions/43299542/value-works-without-configuring-static-propertysourcesplaceholderconfigurer

@Component
@PropertySource("classpath:db.properties")
public class ReadValueFromFile {

	private Logger logger = LoggerFactory.getLogger(Main.class);

	private String url;
		
	public ReadValueFromFile() {
		//in here the object is not instanciated yet
		//appelé avant l'interception des appels par l'aop proxy
		// ==> l'advice n'est pas exécuté
		logger.trace("url constructor : " +  url);
	}
	
	@Value("${myUrl}")
	public void setUrl(String url) {
		//injection de dépendance par setter (se fait aprés la construction de l'objet)
		//appelé avant l'interception des appels par l'aop proxy
		// ==> l'advice n'est pas exécuté
		logger.trace("url setUrl : " +  url);
		this.url = url;
	}
	
	@PostConstruct
	void init() {
		//appelé avant l'interception des appels par l'aop proxy
		// ==> l'advice n'est pas exécuté
		logger.trace("url @PostConstruct : " +  url);
	}
	
	@PreDestroy
	void dest() {
		logger.trace("url @PreDestroy : " +  url);
	}

	public String getUrl() {
		//l'advice est exécuté
		return url;
	}
	
}

