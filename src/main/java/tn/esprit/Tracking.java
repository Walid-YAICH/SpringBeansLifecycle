package tn.esprit;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

@Component //Sinon l'aspect ne sera pas détecté
@Aspect
public class Tracking {
	private Logger logger = LoggerFactory.getLogger(getClass());
	
	@Before("execution(* tn.esprit.ReadValueFromFile.*(..))")
	public void trackCalls(JoinPoint joinPoint){
		logger.info("This Method : " + joinPoint.getSignature().getName() + " is called !");
	}
}
